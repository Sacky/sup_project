### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ fc6209fd-0e56-4d4a-88e0-1fa3e6df870c
using Pkg

# ╔═╡ 54408e2e-313e-4146-aef9-9d8b748e949d
Pkg.activate("Project.toml")

# ╔═╡ 56e58392-c90f-4e4e-b02b-082ab8021213
md"##### Defining Population"

# ╔═╡ dcca79e1-2d4a-465e-8554-f9cd606b05c9
function NUST_IT_Labs(number_of_Labs, map_limit)

    labs = []
    for lab_counter in 1:number_of_Labs
        push!(labs, 
            Dict(
                "id" => lab_counter, 
                "x" => round(rand() * map_limit), 
                "y" => round(rand() * map_limit)
            )
        )
    end
    println("Labs Generated:", size(labs)[1])
    return labs
end

# ╔═╡ 86c03843-aac7-48c9-b28e-e6a1cd969dc1
md"storing the NUST IT Labs as a Dictionary"

# ╔═╡ 974771c9-f542-47fc-bd4a-ecd9ebe2d3ec
md"calling NUST_IT_Labs function to create Labs"

# ╔═╡ e06295de-4e18-452e-b16f-66cec09abb8d
NUST_IT_Labs(12,20)

# ╔═╡ be6e3436-ad3d-4db3-8d50-5526339f7631
md"Representing the solution as chromosome"

# ╔═╡ 07334f6b-d054-40ad-b913-191093cb3b89
[9,12,8,7,6,1,2,3,4,5,11,9]

# ╔═╡ 9ae7768c-618a-46ea-b687-91e4504a1569
md"##### Crossover function"

# ╔═╡ 5379b99c-bc7b-45b0-93d7-59fee65835e7
md" Unipoint crossover"

# ╔═╡ 9da05bdf-5e87-4388-97a2-4a0962e2fbf5
function crossover(first_parent_chromosome, second_parent_chromosome, crossover_point)
    first_offspring = first_parent_chromosome[1:crossover_point]
    for gene in first_offspring
        if gene in second_parent_chromosome
            gene_loc = findfirst(el -> el == gene, second_parent_chromosome)
            splice!(second_parent_chromosome, gene_loc)
        end
    end
    return vcat(first_offspring, second_parent_chromosome)
end

# ╔═╡ e1c79ea6-ad2a-4c7d-9efc-a88adcbc4812
md"##### Mutation function"

# ╔═╡ 4b1b5303-5d4c-4948-80d7-0d7bf1756092
function mutate(offspring)
    random_mutation_point1 = rand(1:length(offspring))
    random_mutation_point2 = rand(1:length(offspring))
    offspring[random_mutation_point1], offspring[random_mutation_point2] = offspring[random_mutation_point2], offspring[random_mutation_point1]
    return offspring
end

# ╔═╡ 58ac2e98-7d55-420e-90ef-eb54ca064e9a
md"##### Fitness function"

# ╔═╡ 5e99a3ee-7d0d-4803-bde9-4af812e05b0e
function calculate_distance_between_two_points(point1, point2)
    return sqrt((((point2[1] - point1[1]))^2) + (((point2[2] - point1[2]))^2))
end

# ╔═╡ 963292df-e0c5-47d7-a844-5030d5f1b0ea
md"##### shuffle the chromosome"

# ╔═╡ 0e19dba2-f9ba-4d1b-ab6e-1bf914ce46e6
function shuffle_chromosome(chromosome)
    for i in 1:size(chromosome)[1]
        random_point = rand(1:12, 1)[1]
        chromosome[i], chromosome[random_point] = chromosome[random_point], chromosome[i]
    end

    println("Created chromosome", chromosome)
    return chromosome
end

# ╔═╡ 65100cb3-95bb-4390-ad80-d32c73d5aad6
md"##### Generating initial population"

# ╔═╡ 412b8c73-5277-426a-a1e6-be431c0e2569
md"creating 10 cities randomly"

# ╔═╡ d8ef19ad-6340-40d8-b584-650492343d38
begin
	labs = NUST_IT_Labs(14, 20)
    initial_chromosome = [2:length(labs);]
end

# ╔═╡ a8cb21ca-d7e6-429a-8ad8-56a1c888292e
function calculate_chromosome_walking_distance(chromosome)
    walking_distance = 0
    chromosome = vcat(1, chromosome, 1)
    for geneId in 1:length(chromosome) - 1
        point1 = (
            labs[chromosome[geneId]]["x"],
            labs[chromosome[geneId]]["y"]
            )
        point2 = (
            labs[chromosome[geneId + 1]]["x"],
            labs[chromosome[geneId + 1]]["y"]
            )
        walking_distance += calculate_distance_between_two_points(point1, point2)
    end
    println("walking distance:", chromosome, " : ", walking_distance)
    return walking_distance
end

# ╔═╡ 702c916a-efef-4fde-9c68-a54f0bd23bec
function generate_initial_population(initial_population_size)
    chromosomes = []
    for population_counter in 1:initial_population_size
        chromosome = shuffle_chromosome(copy(initial_chromosome))
        push!(chromosomes, 
            Dict(
                "chromosome" => chromosome,
                "distance" => calculate_chromosome_walking_distance(chromosome)
            )
        )
    end
    return chromosomes
end

# ╔═╡ d2dbb17f-c834-4fdf-a01a-da42753f2c79
md"generating 14 initial chromosomes"

# ╔═╡ 06b99563-0778-4277-833f-6b0c4f91bbc7
chromosomes = generate_initial_population(12)

# ╔═╡ b7b7918f-17de-49da-98ca-8fe52e45483f
function evolve(generation_count, offsprings_count, crossover_point)
    for generation in 1:generation_count

        for offspring_count in 1:offsprings_count
            println("generation: ", generation, " offspring: ", offspring_count)
            random_first_parent_id = rand(1:size(chromosomes)[1])
            random_second_parent_id = rand(1:size(chromosomes)[1])
            random_first_parent = copy(chromosomes[random_first_parent_id]["chromosome"])
            random_second_parent = copy(chromosomes[random_second_parent_id]["chromosome"])
            offspring = crossover(random_first_parent, random_second_parent, crossover_point)
            offspring = mutate(offspring)
            push!(chromosomes, 
                Dict(
                    "chromosome" => offspring,
                    "distance" => calculate_chromosome_walking_distance(offspring)
                    )
            )
        end
        sort!(chromosomes, by=x -> x["distance"], rev=false)
        splice!(chromosomes, 13:size(chromosomes)[1])
    end
end

# ╔═╡ 8aea43c1-82ba-45ba-bddc-3081b4ada310
md"5 generations
   5 offsprings per generation
   2 random crossover point" 

# ╔═╡ d3fcf012-30b4-4198-953d-70a957d117f8
evolve(5, 5, 2)

# ╔═╡ 6ff0d223-5152-4b61-ba0b-25a9e579b2ad
begin
	println("--------------------------------------------------------")
    println("Optimal route:", vcat(1, chromosomes[1]["chromosome"], 1))
    println("walking_distane:", chromosomes[1]["distance"])
end

# ╔═╡ 16d80d8d-e740-4da7-b6ea-adc60f83aece
#=function evolve(parent::String, target::String, mutrate::Float64, nchild::Int)
    println("Initial parent is $parent, its fitness is $(fitness(parent, target))")
    gens = 0
    while parent != target
        children = collect(mutate(parent, mutrate) for i in 1:nchild)
        bestfit, best = findmax(fitness.(children, target))
        parent = children[best]
        gens += 1
        if gens % 10 == 0
            println("After $gens generations, the new parent is $parent and its fitness is $(fitness(parent, target))")
        end
    end
    println("After $gens generations, the parent evolved into the target $target")
end=#

# ╔═╡ Cell order:
# ╠═fc6209fd-0e56-4d4a-88e0-1fa3e6df870c
# ╠═54408e2e-313e-4146-aef9-9d8b748e949d
# ╠═56e58392-c90f-4e4e-b02b-082ab8021213
# ╠═dcca79e1-2d4a-465e-8554-f9cd606b05c9
# ╠═86c03843-aac7-48c9-b28e-e6a1cd969dc1
# ╠═974771c9-f542-47fc-bd4a-ecd9ebe2d3ec
# ╠═e06295de-4e18-452e-b16f-66cec09abb8d
# ╠═be6e3436-ad3d-4db3-8d50-5526339f7631
# ╠═07334f6b-d054-40ad-b913-191093cb3b89
# ╠═9ae7768c-618a-46ea-b687-91e4504a1569
# ╠═5379b99c-bc7b-45b0-93d7-59fee65835e7
# ╠═9da05bdf-5e87-4388-97a2-4a0962e2fbf5
# ╠═e1c79ea6-ad2a-4c7d-9efc-a88adcbc4812
# ╠═4b1b5303-5d4c-4948-80d7-0d7bf1756092
# ╠═58ac2e98-7d55-420e-90ef-eb54ca064e9a
# ╠═5e99a3ee-7d0d-4803-bde9-4af812e05b0e
# ╠═a8cb21ca-d7e6-429a-8ad8-56a1c888292e
# ╠═963292df-e0c5-47d7-a844-5030d5f1b0ea
# ╠═0e19dba2-f9ba-4d1b-ab6e-1bf914ce46e6
# ╠═65100cb3-95bb-4390-ad80-d32c73d5aad6
# ╠═702c916a-efef-4fde-9c68-a54f0bd23bec
# ╠═b7b7918f-17de-49da-98ca-8fe52e45483f
# ╠═412b8c73-5277-426a-a1e6-be431c0e2569
# ╠═d8ef19ad-6340-40d8-b584-650492343d38
# ╠═d2dbb17f-c834-4fdf-a01a-da42753f2c79
# ╠═06b99563-0778-4277-833f-6b0c4f91bbc7
# ╠═8aea43c1-82ba-45ba-bddc-3081b4ada310
# ╠═d3fcf012-30b4-4198-953d-70a957d117f8
# ╠═6ff0d223-5152-4b61-ba0b-25a9e579b2ad
# ╠═16d80d8d-e740-4da7-b6ea-adc60f83aece
