### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ b2d631be-e842-4e14-9af5-c69d890474de
using Pkg

# ╔═╡ 66b7af88-e3a8-4cda-b638-d5369eb9401a
Pkg.activate("Project.toml")

# ╔═╡ 9e8b45cd-1fa6-4f83-820d-24ec6302997c
using Printf

# ╔═╡ 9087633a-bb58-4a29-96d6-91b1961e72ce
using PlutoUI

# ╔═╡ 7113eec4-8ca5-41bc-b58b-3d29d8d9ba67
using DataStructures

# ╔═╡ 4e128960-dfe3-11eb-2f7c-4b27c1242597
struct constraint_network{T <: Real,U}
    edges::Dict{Tuple{U,U},T}
    verts::Set{U}
	function constraint_network(edges::Vector{Tuple{U,U,T}}) where {T <: Real,U}
    vnames = Set{U}(v for edge in edges for v in edge[1:6])
    adjmat = Dict((edge[1], edge[2], edge[3], edge[4], edge[5], edge[6]) => edge[7] for edge in edges)
    return constraint_network(adjmat, vnames)
	end
end

# ╔═╡ 0cc49177-bc5e-4187-9b67-28f45f66dcbc
struct Constraint
    variables::Vector{String}
    place1::String
    place2::String
    function Constraint(variables::Vector{String}, place1, place2)
        return new(variables, place1, place2)
    end
end

# ╔═╡ 68e473d3-1c35-47ad-9ac9-051d9291a733
function satisfied(constraint::Constraint, assignment)::Bool
    if(!haskey(assignment, constraint.place1) || !haskey(assignment, constraint.place2))
        return true
    end
    return assignment[constraint.place1] != assignment[constraint.place2]
end

# ╔═╡ 7962850f-e525-4268-9c91-09292a6d2fc1
struct CSP
    variables::Vector
    domains::Dict{String, Vector}
    constraints::Dict{String, Vector{}}
	
	function CSP(vars, doms, cons=Dict())
        variables = vars
        domains = doms
        constraints = cons
        for var in vars
            constraints[var] = Vector()
            if (!haskey(domains, var))
                error("Variable should have a domain assigned to it.")
            end
        end
        return new(variables, domains, constraints)
    end
end

# ╔═╡ c087ecb0-bbea-4090-b152-07fe95079484
function add_constraint(csp:: CSP, constraint::Constraint)
    for vari in constraint.variables
        if (!(vari in csp.variables))
            error("Variable in constraint not in CSP")
        else
            push!(csp.constraints[vari], constraint)
        end
    end
end

# ╔═╡ 98580613-1528-4097-b516-3a7ffa3dabd1
function check_consistent(csp:: CSP, variable, assignment)::Bool
    for constraint in csp.constraints[variable]
        if (!satisfied(constraint, assignment))
            return false
        end
        return true
    end
end

# ╔═╡ 3bb35cb1-48bf-4e81-883b-aaa7a0d4716f
function print_sol(d::Dict, pre=1)
    for (k,v) in d
        if typeof(v) <: Dict
            s = "$(repr(k)) => "
            println(join(fill(" ", pre)) * s)
            print_sol(v, pre+1+length(s))
        else
            println(join(fill(" ", pre)) * "$(repr(k)) => $(repr(v))")
        end
    end
    nothing
end

# ╔═╡ 4ff16512-7431-4dfe-8700-d07d4973603b
begin
	vertices(g::constraint_network) = g.verts
    edges(g::constraint_network)    = g.edges 
end

# ╔═╡ 613056cb-a705-4d77-9f70-7b4c89afebba
neighbours(g::constraint_network, v) = Set((X1, X2, X3, X4, X5, X6) for ((X1, X2, X3, X4, X5, X6), X7) in edges(g) if a == v)

# ╔═╡ 7e32c41e-37b6-4a6a-9abc-1113adbcc687
const Domain = [1, 2, 3, 4]

# ╔═╡ 02f7073f-b998-435e-913a-7a5938c1f9d6
const BOARD = raw"""
        X2   
       /  \_______________X5
      / . ---------------//\
     / /                 /  \
    X1  ----------------/....\         X7
     \ \..             /     \\       /
      \   \.......    /       \\     /
       \          \  /         \\   /
        X3---------X4------------X6  
"""

# ╔═╡ c5f2df56-e1b9-436d-94a0-9e770d4365dc
function findpath(g::constraint_network{T,U}, source::U, dest::U) where {T, U}
    @assert source ∈ vertices(g) "$value is not a vertex in the constraint network"
 
    if source == dest return [source], 0 end
    # Initialize variables
    inf  = typemax(T)
    dist = Dict(v => inf for v in vertices(g))
    prev = Dict(v => v   for v in vertices(g))
    dist[source] = 0
    Q = copy(vertices(g))
    neigh = Dict(v => neighbours(g, v) for v in vertices(g))
 
    # Main loop
    while !isempty(Q)
        u = reduce((x, y) -> dist[x] < dist[y] ? x : y, Q)
        pop!(Q, u)
        if dist[u] == inf || u == dest break end
        for (v, cost) in neigh[u]
            alt = dist[u] + cost
            if alt < dist[v]
                dist[v] = alt
                prev[v] = u
            end
        end
    end
	
	# Return path
    rst, cost = U[], dist[dest]
    if prev[dest] == dest
        return rst, cost
    else
        while dest != source
            unshift!(rst, dest)
            dest = prev[dest]
        end
        unshift!(rst, dest)
        return rst, cost
    end
end

# ╔═╡ b7ee1992-e471-4d53-ba55-7ece0f7cae7c
testconstraintnetwork = [("X1", "X2", 2),  ("X1", "X3", 3),  ("X1", "X4", 4), ("X1", "X5", 1),  ("X1", "X6", 2),
             ("X2", "X5", 3), ("X3", "X4", 1), ("X4", "X6", 4),  ("X4", "X5", 9),
             ("X5", "X6", 1), ("X6", "X7", 3)]

# ╔═╡ 0ac0146c-6cee-43ee-a0f4-a8de1839b0ee
g = constraint_network(testconstraintnetwork)

# ╔═╡ 8504c9c9-4d0a-4efb-b3c4-a0e3d36318f0
src, dst = "X5", "X6"

# ╔═╡ 9a21ee4d-059c-455e-af5f-f7723a586c3d
function backtracking_search(csp:: CSP,  assignment=Dict(), path=Dict())::Union{Dict,Nothing}
    
     if length(assignment) == length(csp.variables)
        return assignment
     end

    unassigned::Vector{String} = []
    for v in csp.variables
        if (!haskey(assignment, v))
            push!(unassigned, v)
        end
    end

    # get the every possible domain value of the first unassigned variable
    first = unassigned[1]
    
    for value in csp.domains[first]
        local_assignment = deepcopy(assignment)
        local_assignment[first] = value
        # if we're still consistent, we recurse (continue)
        if check_consistent(csp, first, local_assignment)
            # forward checking, prune future assignments that will be inconsistent
            for un in unassigned
                ass = deepcopy(local_assignment)
                for (i, val) in enumerate(csp.domains[un])
                    ass[un] = val
                    if un != first
                        if(!check_consistent(csp, un, ass))
                            deleteat!(csp.domains[un], i)
                        end
                    end
                end
            end
            path[first] = csp.domains
            
            result = backtracking_search(csp, local_assignment)
            #backtrack if nothing is found
            if result !== nothing
                print_sol(path)
                return result
            end
        end
    end
    return nothing
end


# ╔═╡ 92259115-6fcc-4829-a634-1582eb4861af
begin
	variables = ["X1", "X2", "X3", "X4", "X5", "X6", "X7"]

domains = Dict()
	for variable in variables
		domains[variable] = ["1", "2", "3", "4"]
	end
end

# ╔═╡ 6074541e-ffc5-48ba-ac71-97d5aa071b2e
begin
	game = CSP(variables, domains)
    add_constraint(game, Constraint(["X1", "X2"], "X1", "X2"))
    add_constraint(game, Constraint(["X1", "X3"], "X1", "X3"))
    add_constraint(game, Constraint(["X1", "X5"], "X1", "X5"))
    add_constraint(game, Constraint(["X1", "X6"], "X1", "X6"))
    add_constraint(game, Constraint(["X2", "X5"],"X2", "X5"))
    add_constraint(game, Constraint(["X3", "X4"], "X3", "X4"))
    add_constraint(game, Constraint(["X4", "X5"], "X4", "X5"))
    add_constraint(game, Constraint(["X4", "X6"], "X4", "X6"))
    add_constraint(game, Constraint(["X5", "X6"], "X5", "X6"))
    add_constraint(game, Constraint(["X6", "X7"], "X6", "X7"))
end

# ╔═╡ 70305d50-ea40-4b7f-a9c9-04fb1a6ca5e0
begin
	solution = backtracking_search(game)
	if solution != Nothing
		println("")
		print("solution is: ", solution)
		else
		println("There's no solution")
	end
end

# ╔═╡ f5a1febf-be5b-4513-bed2-dbed117154f4
path, cost = findpath(g, src, dst)

# ╔═╡ 13960c1f-de5e-4619-b46e-6eee4ff433f2
println("Shortest path from $src to $dst: ", isempty(path) ? "no possible path" : join(path, " → "), " (cost $cost)")


# ╔═╡ fedce49c-84c4-4840-a3af-745c17cf1f0c
@printf("\n%4s | %3s | %s\n", "src", "dst", "path")

# ╔═╡ 6e882d8e-ad8b-40e8-b257-240035066fd2
@printf("----------------\n")

# ╔═╡ dd2216d6-640d-477c-b85a-5dcb6cac7f2d
for src in vertices(g), dst in vertices(g)
    path, cost = findapath(g, src, dst)
    @printf("%4s | %3s | %s\n", src, dst, isempty(path) ? "no possible path" : join(path, " → ") * " ($cost)")
end

# ╔═╡ Cell order:
# ╠═b2d631be-e842-4e14-9af5-c69d890474de
# ╠═66b7af88-e3a8-4cda-b638-d5369eb9401a
# ╠═9e8b45cd-1fa6-4f83-820d-24ec6302997c
# ╠═9087633a-bb58-4a29-96d6-91b1961e72ce
# ╠═7113eec4-8ca5-41bc-b58b-3d29d8d9ba67
# ╠═4e128960-dfe3-11eb-2f7c-4b27c1242597
# ╠═0cc49177-bc5e-4187-9b67-28f45f66dcbc
# ╠═68e473d3-1c35-47ad-9ac9-051d9291a733
# ╠═7962850f-e525-4268-9c91-09292a6d2fc1
# ╠═c087ecb0-bbea-4090-b152-07fe95079484
# ╠═98580613-1528-4097-b516-3a7ffa3dabd1
# ╠═3bb35cb1-48bf-4e81-883b-aaa7a0d4716f
# ╠═4ff16512-7431-4dfe-8700-d07d4973603b
# ╠═613056cb-a705-4d77-9f70-7b4c89afebba
# ╠═7e32c41e-37b6-4a6a-9abc-1113adbcc687
# ╠═02f7073f-b998-435e-913a-7a5938c1f9d6
# ╠═c5f2df56-e1b9-436d-94a0-9e770d4365dc
# ╠═b7ee1992-e471-4d53-ba55-7ece0f7cae7c
# ╠═0ac0146c-6cee-43ee-a0f4-a8de1839b0ee
# ╠═8504c9c9-4d0a-4efb-b3c4-a0e3d36318f0
# ╠═9a21ee4d-059c-455e-af5f-f7723a586c3d
# ╠═92259115-6fcc-4829-a634-1582eb4861af
# ╠═6074541e-ffc5-48ba-ac71-97d5aa071b2e
# ╠═70305d50-ea40-4b7f-a9c9-04fb1a6ca5e0
# ╠═f5a1febf-be5b-4513-bed2-dbed117154f4
# ╠═13960c1f-de5e-4619-b46e-6eee4ff433f2
# ╠═fedce49c-84c4-4840-a3af-745c17cf1f0c
# ╠═6e882d8e-ad8b-40e8-b257-240035066fd2
# ╠═dd2216d6-640d-477c-b85a-5dcb6cac7f2d
